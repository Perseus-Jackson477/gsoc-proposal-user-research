
# GSOC Proposal User Research

  
  
  

## Introduction

This is the repository so I can get my GSOC proposal reviewed. Although I will get the other things included, since my project proposal is "Improving CSS Stylesheet to better the UX", I wished to conduct a research and basically get review on the following ideas.
I have taken inspiration for the UX improvement from current popular platforms which employ similar features, like Blender, Visual Studio Code and more
Some of the feartures I had in mind were: 

 - Blender uses a panel below the layers and selector region where users can easily access different options and they can actually see the object/layer they are working upon and then based on that apply different fill styles/CSS thus increasing the UX.  
    Personally when first using inkscape, I did have a bit of trouble on where to locate some of the relevant options, for that we can incorporate a search box on the top and find options needed. See image attatched for reference:
    ![Image](https://i.imgur.com/HAqnSxv.png)
    ![Image](https://i.imgur.com/J834LGu.png)
    
- Next my idea includes including an autofill suggestion for the CSS Stylebox, wherein by typing the initial few letters, one can get suggestions for what probable properties they can add (implementation of a pattern matching algo will do the same)
- We can also incorporate a suggestive drop down menu where someone can get recommendations as well as recently used attributes.
- Another suggestion is including a Color pinwheel (similar to one that people find under Flat color selection) so that the people unfamiliar with hex codes can still find their perfect color choice and see a preview of the color they are about to apply. (Inspired by various CSS extensions present in visual studio code)
![Image](https://i.imgur.com/EnuFMg4.png)

- Export/Import CSS Stylesheet option to allow users to make copies of their CSS stylesheets that they have created so that they can reuse it for different objects and layers
- Give users the ability to implement checkbox manner so they can switch back and forth between different attributes so they can clearly see the impact of a new CSS property to the object (As can be done for various HTML based projects)
![Image](https://i.imgur.com/m2lUwAM.png)